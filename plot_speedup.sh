#!/usr/bin/env python

#Usage: ./plot_speedup.sh hey.txt

import sys
import numpy as np

## We need matplotlib:
## $ apt-get install python-matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

x_Axis = []
time_Axis = []
time1024_Axis = []
time2048_Axis = []
time4096_Axis = []
serial1024 = []
serial2048 = []
serial4096 = []
simplePar1024 = []
simplePar2048 = []
simplePar4096 = []
optPar1024 = []
optPar2048 = []
optPar4096 = []

time1024_Axis.append(serial1024)
time1024_Axis.append(simplePar1024)
time1024_Axis.append(optPar1024)

time2048_Axis.append(serial2048)
time2048_Axis.append(simplePar2048)
time2048_Axis.append(optPar2048)

time4096_Axis.append(serial4096)
time4096_Axis.append(simplePar4096)
time4096_Axis.append(optPar4096)

time_Axis.append(time1024_Axis)
time_Axis.append(time2048_Axis)
time_Axis.append(time4096_Axis)

threads = [1, 2, 4, 8, 16, 32, 64]
sizes = [1024, 2048, 4096]
serial_times = [1.5718, 13.2579, 109.3805]
x_Axis = threads

fp = open(sys.argv[1])
for i in range (7): #We run in 7 thread-sizes for each size
	line = fp.readline()
	for N in range(3): #We run for 3 N's (sizes)
		for j in range(3): #3 algo's
			line = fp.readline()
			tokens = line.split()
			time = float(tokens[5])
			time_Axis[N][j].append(serial_times[N]/time)

fp.close()

algos = ["serial", "simpleParallel", "optParallel"]
N = 0
for N in range(3):
	x_Axis.append(threads[:])
	fig, ax = plt.subplots()
	ax.grid(True)
	ax.set_xlabel("$Threads$")

	ax.xaxis.set_ticks(np.arange(0, len(x_Axis), 1))
	ax.set_xticklabels(x_Axis)
	ax.set_ylabel("$Speedup$")
	ax.plot(time_Axis[N][0], label=algos[0], color="red",marker='x')
	ax.plot(time_Axis[N][1], label=algos[1], color="green",marker='x')
	ax.plot(time_Axis[N][2], label=algos[2], color="black",marker='x')

	handles, labels = ax.get_legend_handles_labels()
	ax.legend(handles, labels)

	plt.title("N = " + str(sizes[N]) + " - naive Floyd Warshall")
	pngName = "naive_" + str(sizes[N]) + "_speedup.png"
	plt.savefig(pngName, bbox_inches="tight")
